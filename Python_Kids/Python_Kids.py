import math

#1. PA E PG
print("1.===================================")
def paepg(n1, nx, r):
    lista = []
    if r%2 == 0:               #caso de razão par --> PA
        while n1 <= nx:        #n1 vai crescer em PA até ultrapassar nx
            lista.append(n1)   #adiciona n1 à cada loop sem esquecer do primeiro valor
            n1 += r            #soma a razão da PA em cada loop
        print(lista)
    
    else:                      #caso de raqzão impar --> PG
        while n1 <= nx:        #n1 vai crescer em PG até ultrapassar nx
            lista.append(n1)   #adiciona n1 à cada loop sem esquecer do primeiro valor
            n1 *= r            #multiplica a razão da PG em cada loop      
        print(lista)
            

a = paepg(0,5000,1000) 
b = paepg(1,1000,3)

#2. Número de 1's na representação binária
print("\n2.===================================")
def oneinbin(num):
    if num < 0:
        print("Escolha um número positivo")
    else:
      a = str(bin(num))[2:].count("1")    #transforma o número desejado em binário e conta o número de 1's
      print(f"A representação binária do número {num} possui {a} números 1")

neg = oneinbin(-1)
teste = oneinbin(500)

#3. De quantas maneiras podemos formas duas libras
print("\n3.===================================")

#4. Ultimos 10 digitos da soma de 1¹ até 1000¹⁰⁰⁰
print("\n4.===================================")
soma = 0
for n in range(1000):
    soma += (n+1)**(n+1)
a = str(soma)[-10:]
print(f"Os ultimos 10 digitos da série de somas são {a}")

