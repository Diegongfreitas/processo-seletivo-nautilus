#define competidores de um campeonato de Jiu-Jitsu
#os pontos definem o número de lutas vencidas, 1 vitória = 1 ponto
import pandas as pd
class Competidor:
    
    def __init__(self, nome, pontos, idade, peso, altura, estilo):
            
        self.nome = nome
        self.pontos = pontos
        self.idade = idade
        self.peso = peso 
        self.altura = altura
        self.estilo = estilo
    
    def __gt__(self, competidor2):
        return (self.pontos > competidor2.pontos)

    def __it__(self,competidor2):
        return (self.pontos < competidor2.pontos)

    def __eq__(self,competidor2):
        return (self.pontos == competidor2.pontos)

    def __ne__(self,competidor2):
        return (self.pontos != competidor2.pontos)

    def __repr__(self):
        return str(self.nome) + ", " + str(self.pontos) + " Pontos, " + str(self.idade) + " Anos, " + str(self.peso) + " Kg, " + str(self.altura) + " metros, " + str(self.estilo) + '\n'

    def __add__(self, competidor2):
        return (self.pontos + competidor2.pontos)

    def __sub__(self, competidor2):
        return (self.pontos - competidor2.pontos)
    
    def __getitem__(self,i):
        return list(self.__dict__.values())[i]


Junior = Competidor("Junior", 3, 18, 68, 1.70, "passador")

Claudio = Competidor("Claudio", 4, 16, 57, 1.65, "guardeiro")

Luciano = Competidor("Luciuano", 2, 20, 75, 1.80, "guardeiro")

Kleber = Competidor("Kleber", 6, 21, 85, 1.84, "passador")

lista = [Junior,Claudio,Luciano,Kleber]
lista.sort() #em ordem de pontos do menor para o maior

print("=================================================================")
print("Competidores em ordem crescente de pontos\n ")

print (pd.DataFrame([list(i)[0:2] for i in lista[::-1]]))
print()
print("Essas são as informações do competidor selecionado: " + str(Junior))

soma = Kleber + Claudio 
print("A soma dos pontos dos competidores é igual a " + str(soma))

sub = Kleber - Claudio 
print("A diferença de pontos dos competidores é igual a " + str(sub))
print("================================================================")