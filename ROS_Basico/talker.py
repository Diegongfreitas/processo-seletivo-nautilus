#!/usr/bin/env python3
from random import random, randint
import rospy
from std_msgs.msg import Float32

def talker():
    pub = rospy.Publisher('distance', Float32, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        hello_str =  Float32()
        hello_str.data = random()*randint(0,10)
        rospy.loginfo(hello_str)
        pub.publish(hello_str)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass