#!/usr/bin/env python3  
import rospy

# Because of transformations
import math
import time
import tf2_ros
import geometry_msgs.msg

def handle_planet_pose(planetname, radium):
    br = tf2_ros.TransformBroadcaster()
    t = geometry_msgs.msg.TransformStamped()

    t.header.stamp = rospy.Time.now()
    t.header.frame_id = "Sol"
    t.child_frame_id = planetname
    t.transform.translation.x = math.cos(time.time()/radium)*radium
    t.transform.translation.y = math.sin(time.time()/radium)*radium
    t.transform.translation.z = 0.0
    t.transform.rotation.x = 0
    t.transform.rotation.y = 0
    t.transform.rotation.z = 0
    t.transform.rotation.w = 1

    br.sendTransform(t)

if __name__ == '__main__':
    rospy.init_node('tf2_planet_broadcaster')
    planetname = rospy.get_param('~planet_names')
    planetradiu = rospy.get_param('~planet_radius')
    while not rospy.is_shutdown():
        for i in range(len(planetname)):
            handle_planet_pose(planetname[i], planetradiu[i])